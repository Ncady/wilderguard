const logger = require('../log.js');
let cooldown = false;
let pvping = false;
module.exports =
    {
        pvpStart: function(message)
        {
            if (pvping === false) {
                message.channel.send(":warning: We are currently PKing. Join the PvP Holding channel! :warning:");
                setTimeout(() => {
                    pvping = false
                }, 7200000);
                pvping = true;
                var interval = setInterval(function () {
                    if (pvping === true) {
                        message.channel.send(":warning: We are currently PKing. Join the PvP Holding channel! :warning:");
                    } else {
                        clearInterval(interval);
                    }
                }, 900000);
            }
            else
            {
                message.channel.send("PvP call already in progress")
            }
        },
        pvpStop: function(message)
        {
            if (pvping === true)
            {
                message.channel.send("Pvp call manually ended");
                pvping = false;
            }
            else
            {
                message.channel.send("No call in progress");
            }
        },
        pvpMass: function(message)
        {

            if (!cooldown) {
                setTimeout(()=> { cooldown = false}, 3600000);
                let role = message.guild.roles.find(role => role.name === "PvP - OSRS");
                role.setMentionable(true, `Mass called by ${message.author.username}` )
                .then(updated => logger.info(`Role : ${role} mentionable: ${updated.mentionable}`));
                message.channel.send(`From ${message.author.username}: <@&${role.id}> ${message.content.substring(9)}`);
                setTimeout(()=>{role.setMentionable(false, `removed ability to mention role` )
                    .then(updated => logger.info(`Role : ${role} mentionable: ${updated.mentionable}`));},10000);
                cooldown = true;
                message.delete();
                lastusage = new Date();
            }
            else {
                let currentdate = new Date();
                let cooldowntime= 3600000 - (currentdate - lastusage);
                let minutes = Math.floor(cooldowntime/60000);
                let seconds = Math.floor((cooldowntime % 60000)/1000);
                message.channel.send(`Command is on cooldown for 1 hour from its last usage\nPlease wait another ${minutes} minutes and ${seconds} seconds`)
            }
        }
    };