const logger=require('../log.js');
module.exports =
{
	roleSearch: function(messageArray,message)
	{
	console.log("Role Search requested : " + messageArray[2]);
	try {
			var roleID = messageArray[2].match(/\d/g); // Regex for "Find a digit"
			roleID = roleID.join("");
			var userCollection = message.guild.roles.get(roleID).members;//filters members by roll ID
			message.channel.send('Users with the ' + messageArray[2] + ' role:');
			userList = userCollection.map((u) =>
				{
					return u.user.id
				});
			const chunk = 40; // Number of members to print per message (Character Limit is 2000 per message)
			let i, j, temp;
			for (i = 0, j = userList.length; i < j; i += chunk)
				{
					temp = userList.slice(i, i + chunk);
					printList = temp.map((u) =>
						{
							return '<@' + u + '>'
						});
					message.channel.send(printList);
				}
		}
		catch (err)
		{
			message.channel.send("Invalid role");
			console.log("Invalid role");
		}
	},
	editPrefix: function(messageArray, message)
		{
			console.log("Attempting prefix change");
			try
				{
					if (messageArray[2].length === 1)
						{
							let fs = require('fs');
							let json = require('../settings.json');
							json.prefix = messageArray[2];
							fs.writeFile('Bot/settings.json', JSON.stringify(json, null, 5), (err) =>
								{
									console.log(err || "Prefix changed to :" + messageArray[2]);
								});
							message.channel.send("Prefix has been changed to : " + messageArray[2])
						}
					else
						{
							message.channel.send("Invalid Syntax, One character maximum")
						}
					}
			catch (err)
				{
					console.log(err);
					message.channel.send("Failed to update prefix");
					console.log("Failed to update prefix");
				}
		},
	editStatus: function(messageArray, message, bot)
		{
			console.log("Attempting status change");
			try
				{
					let fs = require('fs');
					let json = require('../settings.json');
					json.statusMessage = message.content.substring(13); // Takes message after !edit status
					fs.writeFile('Bot/settings.json', JSON.stringify(json, null, 5), (err) =>
						{
							console.log(err || "Status changed to :" + json.statusMessage);
						});
					message.channel.send("Status has been changed to : " + json.statusMessage);
					bot.user.setActivity(json.statusMessage, {type: 'WATCHING'})
				}
			catch (err)
				{
					console.log(err);
					message.channel.send("Failed to update status");
					console.log("Failed to update status");
				}
		},
	editLog: function(messageArray, message)
		{
			console.log("Attempting to change log channel");
			try
				{
					let fs = require('fs');
					let json = require('../settings.json');
					if (isNaN(messageArray[2]))
						{
							message.channel.send("Invalid Channel ID");
							return
						}
					json.logChannel = messageArray[2];
					fs.writeFile('Bot/settings.json', JSON.stringify(json, null, 5), (err) =>
						{
							console.log(err || "Log Channel reasigned to :" + messageArray[2]);
						});
					message.channel.send("log Channel has been reasigned to : " + messageArray[2])
				}
			catch (err)
				{
					console.log(err);
					logger.error(err);
					message.channel.send("Failed to update log Channel");
					console.log("Failed to update Log Channel");
				}
		}
};