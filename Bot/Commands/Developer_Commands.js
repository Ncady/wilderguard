const logger=require('../log.js');
module.exports =
    {
        getVersion: function(messageArray,message)
        {
            try
            {
                let json = require('../package.json');
                message.channel.send("Current Version is : "+ json.version);
            }
            catch(err)
            {
                logger.error("Failed to get version number");
                message.channel.send("Failed to get version number.");
            }
        },
        getUptime: function(channel)
        {
            let totalSeconds = (process.uptime());
            let days = Math.floor(totalSeconds / 86400);
                totalSeconds %= 86400;
            let hours = Math.floor(totalSeconds / 3600);
                totalSeconds %= 3600;
            let minutes = Math.floor(totalSeconds / 60);
            let seconds = Math.floor(totalSeconds % 60);
            channel.send(`${days} days, ${hours} hours, ${minutes} minutes and ${seconds} seconds`)
        }
    };

