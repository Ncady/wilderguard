const adminCommands = require('../Commands/Admin_Commands.js');
const developerCommands = require('../Commands/Developer_Commands.js');
const osrsCommands = require('../Commands/OSRS_Commands.js');
const permission = require('../permissions.json'); // Role Permissions
module.exports =
    {
    checkCmd: function (messageArray, message, bot)
    {
        permLevel = module.exports.checkPermission(message);
        //console.log(permLevel); // log highest permission level
        switch (messageArray[0])
        {
            case 'get':
                switch (messageArray[1]){
                    case 'role':
                         if (permLevel >= 3) {
                            adminCommands.roleSearch(messageArray, message);
                         }
                         else {
                            message.channel.send("insufficient permission");
                         }
                         break;
                    case 'version':
                        developerCommands.getVersion(messageArray,message);
                        break;
                    case'uptime':
                        developerCommands.getUptime(message.channel);
                        break;
                }
                break;
            case 'edit':
                switch (messageArray[1]) {
                    case 'prefix':
                        if (permLevel >= 3) {
                            adminCommands.editPrefix(messageArray, message);
                        }
                        else {
                            message.channel.send("insufficient permission");
                        }
                        break;
                    case 'log':
                        if (permLevel >= 3) {
                            adminCommands.editLog(messageArray, message);
                        }
                        else {
                            message.channel.send("insufficient permission");
                        }
                        break;
                    case 'status':
                        if (permLevel >= 3) {
                            adminCommands.editStatus(messageArray, message, bot);
                        }
                        else {
                            message.channel.send("insufficient permission");
                        }
                        break;
                }
                break;
            case 'pvpmass':
                if (message.member.roles.some(role => role.name === "Old School" || role.name === "Bot Dev")){
                    osrsCommands.pvpMass(message);
                }
                else {
                    message.channel.send("insufficient permission");
                }
                break;
            case 'pvpstart':
            case 'pvp-start':
                if (message.member.roles.some(role => role.name === "Head Warlord" ||
                    role.name === "Bot Dev" ||
                    role.name === "Leader - OSRS" ||
                    role.name === "Advisor - OSRS" ||
                    role.name === "App Manager - OSRS" ||
                    role.name === "Eventmaster" ||
                    role.name === "Warlord - OSRS" ||
                    role.name === "Officer - OSRS"
                )){
                    osrsCommands.pvpStart(message);
                }
                else {
                    message.channel.send("insufficient permission");
                }
                break;
            case 'pvpstop':
            case 'pvp-stop':
            if (message.member.roles.some(role => role.name === "Head Warlord" ||
                role.name === "Bot Dev" ||
                role.name === "Leader - OSRS" ||
                role.name === "Advisor - OSRS" ||
                role.name === "App Manager - OSRS" ||
                role.name === "Eventmaster" ||
                role.name === "Warlord - OSRS" ||
                role.name === "Officer - OSRS"
            )){
                osrsCommands.pvpStop(message);
            }
            else
            {
                message.channel.send("insufficient permission");
            }
            break;
        }
    },
    checkPermission: function (message)
    {
        let highestPerm = 0;
        let msgAuthor = message.member;
        let roleList = msgAuthor.roles.map(r => `${r.name}`);
        for(let i = 0; i < permission.roles.length; i++)
        {
            for(let j = 0; j < roleList.length; j++)
            {
                if (permission.roles[i].name === roleList[j])
                {
                    if (permission.roles[i].permLevel > highestPerm)
                    {
                        highestPerm = permission.roles[i].permLevel
                    }
                }
            }
        }
        return highestPerm;
    }
};