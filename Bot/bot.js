﻿const Discord = require('discord.js');
const bot = new Discord.Client();
const exists = require('./Utilities/Command_Check.js');
const logger=require('./log.js');
const auth = require('./auth.json'); // Bot Auth token
let settings = require('./settings.json'); // Bot settings
bot.login(auth.token);
bot.on('error', (e) =>
	{
		logger.error(e)
	});
bot.on('ready', () => 
	{
		bot.user.setActivity(settings.statusMessage, { type: 'WATCHING' })
			.catch(logger.error);
		logger.info('Connected');
		logger.info('Logged in as: '+bot.user.username +' -(#' + bot.user.id + ')');
	});
bot.on('guildMemberUpdate', async (oldMember, newMember) =>
	{
		if (newMember.roles.find(role => role.name === "Old School") && !oldMember.roles.find(role => role.name === "Old School"))
		{
			const entry = await newMember.guild.fetchAuditLogs({type: 'MEMBER_ROLE_UPDATE'}).then(audit => audit.entries.first());
			bot.channels.get(settings.logChannel).send(newMember + ' was given the Old School role by '+ entry.executor.username); //Casts message to log channel on member recieving OSRS role
		}
	});
bot.on('guildMemberRemove',(member)=>
	{
		if (member.roles.find(role => role.name === "Old School" || role.name === "RuneScape 3"))
		{
			let roleList = "";
			const embed = new Discord.RichEmbed()
				.setAuthor('Wilderguard', 'https://cdn.discordapp.com/avatars/524340851066011664/e899ab01c6bd85a4409140a5930a25c9.png', 'https://www.wildernessguardians.com/')
				.setTitle('Ranked member leave')
				.setColor(0xFF0000)
				.setDescription(member.user.tag+'('+member.displayName+') with the following watched roles has left the server')
				.setTimestamp();
			if (member.roles.find(role => role.name === "Old School"))
			{
				roleList = "Old School\n"
			}
			if (member.roles.find(role => role.name === "RuneScape 3"))
			{
				roleList += "RuneScape 3";
			}
			embed.addField("Watched roles",roleList);
			if( member.roles.find(role => role.name === "Old School" && role.name === "RuneScape 3"))
			{
				bot.channels.get(settings.WGStaff).send(embed);
			}
			else if (member.roles.find(role => role.name === "Old School"))
			{
				bot.channels.get(settings.AppManager).send(embed); //Casts message to log channel on member recieving OSRS role
			}
			else if (member.roles.find(role => role.name === "RuneScape 3"))
			{
				bot.channels.get(settings.RuneScape3Staff).send(embed);
			}
		}
	});
bot.on('message', async message => 
	{
	    let msg = message.content.toLowerCase();
	    if(message.author.bot) // Ignores other bots
	    	{
	    		return;
	    	}
	    if(msg.charAt(0) !== (settings.prefix)) // Checks to make sure the message includes the prefix specificed in settings.json
	    	{
	    		return;
	    	}
	    msg = msg.substr(1);
	    let messageArray = msg.split(' ');
	    //console.log(message.content+' '+message.author.tag+ ' <'+message.author.id+'>'); // debug line: echos all messages along with user who sent them to console
		exists.checkCmd(messageArray,message,bot)
	});

