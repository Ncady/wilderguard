const winston = require('winston');
let now = new Date();
const logger = new winston.createLogger();

             logger.add(new winston.transports.Console(
                    {
                        format: winston.format.combine(
                            winston.format.colorize(),
                            winston.format.simple()
                        )
                    }));
                logger.add(new winston.transports.File(
                    {
                        level: 'info',
                        colorize: false,
                        timestamp: true,
                        json: true,
                        filename: './Logs/Info_Logs/Info-'+now.getMonth()+'-'+now.getDay()+'-'+now.getFullYear()+'-('+now.getHours()+'-'+now.getMinutes()+'-'+now.getSeconds()+').log',
                        handleExceptions: true
                    }));
                logger.add(new winston.transports.File(
                    {
                        level:'error',
                        filename: './logs/Error_Logs/Errors-'+now.getMonth()+'-'+now.getDay()+'-'+now.getFullYear()+'-('+now.getHours()+'-'+now.getMinutes()+'-'+now.getMilliseconds()+').log',
                        timestamp: true
                    }));
module.exports=logger;